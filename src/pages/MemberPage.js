import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Columns,
	Heading
} from 'react-bulma-components';

import MemberAdd from '../components/MemberAdd';
import MemberList from '../components/MemberList';


class MemberPage extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Members</Heading>
				<Columns>
					<MemberAdd />
					<MemberList />
				</Columns>
			</Section>
		)
	}
}

export default MemberPage;
