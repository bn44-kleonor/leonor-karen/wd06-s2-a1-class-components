import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

import MemberPage from './pages/MemberPage';
import NavbarPage from './components/AppNavbar';

const root = document.querySelector("#root");

const pageComponent = (
	<Fragment>
		<NavbarPage />
		<MemberPage />
	</Fragment>
);

ReactDOM.render(pageComponent, root);
