import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Navbar
} from 'react-bulma-components';

class NavbarPage extends Component {
	render()
	{
		return (
			<Navbar className="is-black">
				<Navbar.Brand>
					<Navbar.Item>
						<strong>MERNG Tracker</strong>
					</Navbar.Item>
					<Navbar.Burger />
				</Navbar.Brand>
				<Navbar.Menu>
					<Navbar.Container>
						<Navbar.Item>Members</Navbar.Item>
						<Navbar.Item>Teams</Navbar.Item>
						<Navbar.Item>Tasks</Navbar.Item>
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
		)
	}
}

export default NavbarPage;
