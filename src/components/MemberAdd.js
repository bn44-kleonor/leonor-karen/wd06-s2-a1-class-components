import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Heading,
	Columns,
	Card
} from 'react-bulma-components';


class MemberAdd extends Component {
	render()
	{
		return (
			<Columns.Column size={4}>
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Add Member
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<form>
							<div className="field">
							  <label className="label">First Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div className="field">
							  <label className="label">Last Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div className="field">
							  <label className="label">Position Name</label>
							  <div className="control">
							    <input className="input" type="text" />
							  </div>
							</div>
							<div class="field">
							  <label class="label">Team</label>
							  <div class="control">
							    <div class="select is-fullwidth">
							      <select>
							        <option>Select team</option>
							        <option>With options</option>
							      </select>
							    </div>
							  </div>
							</div>
							<br/>
							<div class="field">
							  <div class="control">
							    <button class="button is-link is-success">Add</button>
							  </div>
							</div>
						</form>
					</Card.Content>
				</Card>
			</Columns.Column>
		)
	}
}

export default MemberAdd;
