import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns,
	Card
} from 'react-bulma-components';


class MemberList extends Component {
	render()
	{
		return (
					<Columns.Column>
						<Card>
							<Card.Header>
								<Card.Header.Title>
									Member List
								</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<table className="table is-fullwidth is-bordered is-striped is-hoverable">
									<thead>
										<th>Member Name</th>
										<th>Position</th>
										<th>Team</th>
										<th>Action</th>
									</thead>
									<tbody>
										<tr>
											<td>Sample</td>
											<td>Sample</td>
											<td>Sample</td>
											<td>Sample</td>
										</tr>
									</tbody>
								</table>
							</Card.Content>
						</Card>
					</Columns.Column>
		)
	}
}

export default MemberList;
